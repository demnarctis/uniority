CREATE TABLE `idea` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(255) NOT NULL,
  `description` TEXT NOT NULL,
  `user` INT NULL DEFAULT NULL,
  PRIMARY KEY (`id`));

ALTER TABLE `idea` 
ADD INDEX `fk_idea_user_idx` (`user` ASC);
ALTER TABLE `idea` 
ADD CONSTRAINT `fk_idea_user`
  FOREIGN KEY (`user`)
  REFERENCES `user` (`id`)
  ON DELETE NO ACTION
  ON UPDATE CASCADE;

