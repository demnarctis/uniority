<?php

/**
 * @todo Short description
 * Implement method to get short description.
 * Use this method to show short description in ideas list page.
 */

namespace app\controllers;

use Yii;
use app\models\Idea;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\HttpException;

/**
 * IdeaController implements the CRUD actions for Idea model.
 */
class IdeaController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'index'  => ['get'],
					'view'   => ['get'],
					'create' => ['get', 'post'],
					'update' => ['get', 'put', 'post'],
					'delete' => ['post', 'delete']
                ]
            ],
			'access' => [
				'class' => \yii\filters\AccessControl::className(),
				'only' => ['create', 'update', 'delete'],
				'rules' => [
					[
						'allow' => true,
						'roles' => ['@']
					]
				]
			]
        ];
    }

    /**
     * Lists all Idea models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Idea::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Idea model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'idea' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Idea model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $idea = new Idea();
		$idea->user = Yii::$app->user->id;
        if ($idea->load(Yii::$app->request->post()) && $idea->save()) {
            return $this->redirect(['view', 'id' => $idea->id]);
        } else {
            return $this->render('create', [
                'idea' => $idea,
            ]);
        }
    }

    /**
     * Updates an existing Idea model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
		
        $idea = $this->findModel($id);
		
		if ( ! (Yii::$app->user->id == $idea->user)) throw new HttpException(403, 'У вас нет прав на выполнение этого действия!');
		
        if ($idea->load(Yii::$app->request->post()) && $idea->save()) {
            return $this->redirect(['view', 'id' => $idea->id]);
        } else {
            return $this->render('update', [
                'idea' => $idea,
            ]);
        }
    }

    /**
     * Deletes an existing Idea model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
		
        $idea = $this->findModel($id);
		if ( ! (Yii::$app->user->id == $idea->user)) throw new HttpException(403, 'У вас нет прав на выполнение этого действия!');
		$idea->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Idea model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Idea the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($idea = Idea::findOne($id)) !== null) {
            return $idea;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
