<?php
/* @var $this yii\web\View */
$this->title = 'Uniority | Homepage';
?>

<section class="main">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="jumbotron">
                  <h2>Welcome to Uniority!</h2>
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fuga autem necessitatibus obcaecati excepturi eligendi praesentium tenetur a eum ex, numquam, sapiente dolorum magnam debitis error? Magnam sequi harum fuga eum ut nesciunt sit nobis deleniti, soluta, qui nisi cupiditate mollitia rerum ullam repellat labore vitae sunt. Aliquam accusamus, dolorem praesentium placeat repellendus nulla quidem explicabo, aperiam quis, fuga excepturi minima in! Inventore blanditiis ratione modi, saepe id voluptates eveniet, at totam consequuntur reiciendis beatae distinctio dolorum laboriosam non aperiam.</p>
                  <p><a class="btn btn-primary btn-lg" href="#" role="button">Принять участие</a></p>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="about">
        <div class="row">
            <div class="col-md-4 col-lg-4 col-xs-12 col-sm-4 block">
                <i class="fa fa-cogs fa-4x"></i>
                <h4>Придумывайте идеи!</h4>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odio culpa voluptas quidem sunt fuga aliquam omnis exercitationem, magni, at beatae, architecto voluptates, harum aliquid veritatis sapiente nemo soluta temporibus ab.
                </p>
            </div>
            <div class="col-md-4 col-lg-4 col-xs-12 col-sm-4 block">
                <i class="fa fa-users fa-4x"></i>
                <h4>Делитесь с сообществом!</h4>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odio culpa voluptas quidem sunt fuga aliquam omnis exercitationem, magni, at beatae, architecto voluptates, harum aliquid veritatis sapiente nemo soluta temporibus ab.
                </p>
            </div>
            <div class="col-md-4 col-lg-4 col-xs-12 col-sm-4 block">
                <i class="fa fa-code-fork fa-4x"></i>
                <h4>Разрабатывайте вместе!</h4>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sit voluptatem ut in ullam. Adipisci eum praesentium ut soluta enim aspernatur accusantium suscipit officia eos vel!
                </p>
            </div>
        </div>
</section>
