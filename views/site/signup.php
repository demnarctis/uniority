<?php 
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
 ?>

<?php
$this->title = 'Signup | Register';
?>

<section class="signup">
	<div class="site-signup">
		<div class="row">
			<div class="col-lg-5 col-lg-offset-3">
				<h1>Signup,user!</h1>
				<?php $form = ActiveForm::begin(['id'=>'form-signup']);?>
				<?= $form->field($model,'username') ?>
				<?= $form->field($model,'email') ?>
				<?= $form->field($model,'password') ?>
				<div class="form-group">
					<?= Html::submitButton('Register',['class'=>'btn btn-primary'])?>
				</div>
				<?php ActiveForm::end();?>
			</div>
		</div>
	</div>
</section>