<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Банк Идей';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="idea-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= ! Yii::$app->user->isGuest ? Html::a('Добавить идею', ['create'], ['class' => 'btn btn-success']) : ''?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
		
        'columns' => [
            [
				'attribute' => 'title',
				'contentOptions' => [
					'width' => '35%'
				],
			],
            'description:ntext',
            [
				'class' => 'yii\grid\ActionColumn',
				'buttons' => 
				[
					'update' => function ($url, $idea) {
						if($idea->user == Yii::$app->user->id)
						{
							return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
								'title' => Yii::t('yii', 'Update'),
								'data-pjax' => '0'
							]);
						}
						else
						{
							return '';
						}
					},
					'delete' => function ($url, $idea) {
						if($idea->user == Yii::$app->user->id)
						{
							return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
								'title' => Yii::t('yii', 'Delete'),
								'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
								'data-method' => 'post',
								'data-pjax' => '0',
							]);
						}
						else
						{
							return '';
						}
						
					}
				]
				
			]
        ]
    ]); ?>

</div>
