<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $idea app\models\Idea */

$this->title = 'Редактирование';
$this->params['breadcrumbs'][] = ['label' => 'Банк Идей', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $idea->title, 'url' => ['view', 'id' => $idea->id]];
$this->params['breadcrumbs'][] = 'Редактировать';
?>
<div class="idea-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'idea' => $idea,
    ]) ?>

</div>
