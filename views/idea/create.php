<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $idea app\models\Idea */

$this->title = 'Добавить идею';
$this->params['breadcrumbs'][] = ['label' => 'Банк Идей', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="idea-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'idea' => $idea,
    ]) ?>

</div>
