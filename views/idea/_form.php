<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $idea app\models\Idea */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="idea-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($idea, 'title')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($idea, 'description')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton($idea->isNewRecord ? 'Create' : 'Update', ['class' => $idea->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
