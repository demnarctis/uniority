<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $idea app\models\Idea */

$this->title = $idea->title;
$this->params['breadcrumbs'][] = ['label' => 'Банк Идей', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="idea-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?php
		if( Yii::$app->user->id == $idea->user)
		{
			echo Html::a('Редактировать', ['update', 'id' => $idea->id], ['class' => 'btn btn-primary']);
			echo ' ';
			echo Html::a('Удалить', ['delete', 'id' => $idea->id], [
				'class' => 'btn btn-danger',
				'data' => [
					'confirm' => 'Вы уверены, что хотите удалить идею?',
					'method' => 'post',
				],
			]);
		}
			?>
    </p>

    <?= DetailView::widget([
        'model' => $idea,
        'attributes' => [
            'title',
            'description:ntext',
            'user'
        ]
    ]) ?>

</div>
