<?php

use yii\db\Schema;
use yii\db\Migration;

class m141129_212225_create_user_table extends Migration
{
    public function up()
    {
    	$this->createTable('user',[
    		'id'=>'pk',
    		'username'=>Schema::TYPE_STRING,
    		'password'=>Schema::TYPE_STRING,
    		'email'=>Schema::TYPE_STRING,
    		'password_hash'=>Schema::TYPE_STRING,
    		'auth_key'=>Schema::TYPE_STRING,
    	]);
    }

    public function down()
    {
        $this->dropTable('user');
    }
}
