<?php

use yii\db\Schema;
use yii\db\Migration;

class m141129_210958_create_profile_table extends Migration
{
    public function up()
    {
    	$this->createTable('profile', [
    		'id'=>'pk',
    		'firstName'=>Schema::TYPE_STRING . ' NOT NULL',
    		'secondName'=>Schema::TYPE_STRING,
    		'user_id'=>Schema::TYPE_INTEGER,
    	]);
    }

    public function down()
    {
        $this->dropTable('profile');
    }
}
