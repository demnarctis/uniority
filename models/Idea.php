<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "idea".
 *
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property integer $user
 *
 * @property User $userDetails
 */
class Idea extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'idea';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'description'], 'required'],
            [['description'], 'string'],
            [['user'], 'integer'],
            [['title'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Заголовок',
            'description' => 'Описание',
			'user' => 'Пользователь'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserDetails()
    {
        return $this->hasOne(User::className(), ['id' => 'user']);
    }
}
