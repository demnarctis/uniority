<?php

namespace app\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $name
 * @property string $email
 * @property string $password
 */
class User extends ActiveRecord implements IdentityInterface
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            //[['name', 'email', 'password'], 'required'],
            //[['name', 'email', 'password'], 'string', 'max' => 255],
            //[['email'],'email'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Name',
            'email' => 'Email',
            'password' => 'Password',
        ];
    }

    public static function findByEmail($email)
    {
        return static::findOne(['email'=>$email]);
    }

    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password,$this->password_hash);
    }

    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }
     public static function findIdentity($id)
    {
        return static::findOne(['id' => $id]);
    }
public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }
// public static function findByPasswordResetToken($token)
//     {
//         if (!static::isPasswordResetTokenValid($token)) {
//             return null;
//         }

//         return static::findOne([
//             'password_reset_token' => $token,
//         ]);
//     }
// public static function isPasswordResetTokenValid($token)
//     {
//         if (empty($token)) {
//             return false;
//         }
//         $expire = Yii::$app->params['user.passwordResetTokenExpire'];
//         $parts = explode('_', $token);
//         $timestamp = (int) end($parts);
//         return $timestamp + $expire >= time();
//     }
    public function getId()
        {
            return $this->getPrimaryKey();
        }
    public function getAuthKey()
        {
            return $this->auth_key;
        }
    public function validateAuthKey($authKey)
        {
            return $this->getAuthKey() === $authKey;
        }
    public function generateAuthKey()
        {
            $this->auth_key = Yii::$app->security->generateRandomString();
        }
    // public function generatePasswordResetToken()
    //     {
    //         $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    //     }
    // public function removePasswordResetToken()
    //     {
    //         $this->password_reset_token = null;
    //     }
}
