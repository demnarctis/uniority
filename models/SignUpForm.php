<?php
namespace app\models;

use Yii;
use yii\base\Model;
use app\models\User;

class SignUpForm extends Model{

    public $username;
    public $email;
    public $password;

    public function rules()
    {
        return [
            ['username','filter','filter'=>'trim'],
            ['username','required'],
            ['username','string','min'=>2,'max'=>20],

            ['email','filter','filter'=>'trim'],
            ['email','required'],
            ['email','email'],
            ['email','unique','targetClass'=>'\app\models\User','message'=>'This email is already used'],

            ['password','required'],
            ['password','string','min'=>5],
        ];
    }

    public function signup()
    {
        if($this->validate())
        {
            $user = new User;
            $user->username = $this->username;
            $user->email = $this->email;
            $user->setPassword($this->password);
            $user->generateAuthKey();
            $user->save();
            if ($user->hasErrors()) {
                var_dump($user->getErrors());
            }
            return $user;
        }
        return null;
    }

}
?>