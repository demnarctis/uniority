/**
 * Script for menu control
 */
$(document).ready( function()
{
	$('.dropdown').on('mouseover', Menu.HightlightOn);
	$('.dropdown').on('mouseout', Menu.HightlightOff);
	$('.dropdown > a').on('click', Menu.DisableClick);
});

var Menu = 
{
	HightlightOn: function(event) 
	{
		if($(this).find('.dropdown-menu').is(':visible')) 
		{
			$(this).addClass('hover');
		}
	},
	HightlightOff: function(event) 
	{
		if($(this).find('.dropdown-menu').not(':visible')) 
		{
			$(this).removeClass('hover');
		}
	},
	DisableClick: function(event)
	{
		return false;
	}
}